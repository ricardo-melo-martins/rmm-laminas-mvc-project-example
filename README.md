# RMM Laminas MVC with Sakila Database Sample

Boilerplate template with Sakila Module Example

Template padrão para iniciar projetos com modulo de exemplo Sakila criado para usar com o banco de dados Sakila

Este padrão foi criado usando 
- PHP 7.3 e 8.0
- Bootstrap
- Banco de dados Mysql

E recursos do Laminas MVC
- Paginator
- Translator

# Porque

Sempre gostei de trabalhar com o Zend e Apigility, me proporcionou aprender sobre camadas de desenvolvimento, DDD, Injeção de dependência e Service Layer num momento onde outros frameworks não tinham esta cultura.

Tive a oportunidade em criar e manter projetos nas versões do Zend do 1 ao 3 em grandes empresas, mas de uns anos pra cá os projetos do qual participei usavam outros frameworks e até outras linguagens. Resolvi então fazer uma revisita e experimentá-lo agora que se chama Laminas, além de reviver experiências vou tentar contribuir de alguma forma com o projeto criando este modelo e tentando resolver problemas ou dúvidas.


# Changelog

Todas as mudanças notáveis neste projeto serão documentadas neste arquivo, em ordem cronológica reversa por lançamento. 

## 1.0.0 - 2021-07-30

- Criado o projeto esqueleto a partir do laminas-mvc-skeleton seguindo o tutorial https://docs.laminas.dev/tutorials/getting-started/skeleton-application/, mas criar um modulo Sakila no lugar de Album.
- Integrado ao banco de dados [RMM-Docker-Database-Samples] Sakila usando o componente laminas-db
- usado a tabela Film do banco de dados Sakila no lugar do Album para criar o CRUD.
- Tela para listagem de filmes existentes na tabela Film
- Na listagem de filmes, usado paginação criada com o componente laminas-paginator
- Criado menu de navegação usando laminas-navigation
- Criado menu breadcrumb usando laminas-navigation
- Criado casos de testes para CRUD de Film
- 

