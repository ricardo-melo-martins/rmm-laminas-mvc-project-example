<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */


return [
    'db' => [
        'driver' => 'Pdo_Mysql',
        'hostname' => 'localhost',
        'database' => 'sakila',
        'driver_options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ],
    ],
];


/*
ref. https://docs.laminas.dev/laminas-db/adapter/

The list of officially supported drivers:

    IbmDb2: The ext/ibm_db2 driver
    Mysqli: The ext/mysqli driver
    Oci8: The ext/oci8 driver
    Pgsql: The ext/pgsql driver
    Sqlsrv: The ext/sqlsrv driver (from Microsoft)
    Pdo_Mysql: MySQL via the PDO extension
    Pdo_Sqlite: SQLite via the PDO extension
    Pdo_Pgsql: PostgreSQL via the PDO extension
*/
