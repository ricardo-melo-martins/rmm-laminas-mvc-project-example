<?php

namespace Sakila;

use Laminas\Router\Http\Segment;
use Laminas\Db\Adapter\AdapterAbstractServiceFactory;

return [

    'service_manager' => [
        'factories' => [
            // 'Sakila\Db\WriteReadAdapter' => AdapterAbstractServiceFactory::class,
            'Laminas\Db\Adapter\Adapter' => 'Laminas\Db\Adapter\AdapterServiceFactory',
            'Laminas\Db\TableGateway\TableGateway' => 'Laminas\Db\TableGateway\TableGatewayServiceFactory',
        ],
    ],

    'router' => [
        'routes' => [
            'film' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/sakila/film[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\FilmController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],

        
    'view_manager' => [
        'template_path_stack' => [
            'sakila' => __DIR__ . '/../view',
        ],
    ],
];