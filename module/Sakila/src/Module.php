<?php
namespace Sakila;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    
    public function getServiceConfig()
    {
        return [
            'factories' => [
                
                // Configuro as models para acesso ao banco
                Model\FilmTable::class => function($container) {
                    $tableGateway = $container->get(Model\FilmTableGateway::class);
                    return new Model\FilmTable($tableGateway);
                },

                Model\FilmTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Film());
                    return new TableGateway('film', $dbAdapter, null, $resultSetPrototype);
                }
            ]
        ];
    }

    
    public function getControllerConfig()
    {
        return [
            'factories' => [
                // Configura a controladora injetando o serviço
                Controller\FilmController::class => function ($container) {
                    return new Controller\FilmController(
                        $container->get(Model\FilmTable::class)
                    );
                }
            ]
        ];
    }
}