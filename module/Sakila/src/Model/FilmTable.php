<?php

namespace Sakila\Model;

use RuntimeException;
use Laminas\Db\TableGateway\TableGatewayInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\Sql\Select;
use Laminas\Paginator\Adapter\DbSelect;
use Laminas\Paginator\Paginator;

class FilmTable
{

    private $tableGateway;

    
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    
    public function fetchAll($paginated = false)
    {
        if ($paginated) {
            return $this->fetchPaginatedResults();
        }

        return $this->tableGateway->select();
    }

    
    private function fetchPaginatedResults()
    {
        // Create a new Select object for the table:
        $select = new Select($this->tableGateway->getTable());

        // Create a new result set based on the Film entity:
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Film());

        // Create a new pagination adapter object:
        $paginatorAdapter = new DbSelect(
            // our configured select object:
            $select,
            // the adapter to run it against:
            $this->tableGateway->getAdapter(),
            // the result set to hydrate:
            $resultSetPrototype
        );

        return new Paginator($paginatorAdapter);
    }

    
    public function getFilm($id)
    {
        $id = (int) $id;

        $rowset = $this->tableGateway->select(['film_id' => $id]);
        $row = $rowset->current();
        
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    
    public function saveFilm(Film $film)
    {
        
        $data = [
            'title'       => $film->title,
            'description' => $film->description,
            'language_id' => $film->language_id,
        ];

        $id = (int) $film->film_id;

        if ($id === 0) {
            $this->tableGateway->insert($data);
            return;
        }

        try {
            $this->getFilm($id);
        } catch (RuntimeException $e) {
            throw new RuntimeException(sprintf(
                'Cannot update film with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['film_id' => $id]);
    }

    public function deleteFilm($id)
    {
        $this->tableGateway->delete(['film_id' => (int) $id]);
    }
}