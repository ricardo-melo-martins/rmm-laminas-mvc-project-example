<?php

namespace Sakila\Model;

use DomainException;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Filter\ToInt;
use Laminas\InputFilter\InputFilter;
use Laminas\InputFilter\InputFilterAwareInterface;
use Laminas\InputFilter\InputFilterInterface;
use Laminas\Validator\StringLength;

class Film
{
    public $film_id;
    public $title;
    public $description;
    public $language_id;
    public $release_year;
    public $length;
    public $last_update;
    public $original_language_id;
    public $rental_duration;
    public $rental_rate;
    public $replacement_cost;
    public $rating;
    public $special_features;

    public $inputFilter;

    public function exchangeArray(array $data)
    {
        $this->film_id      = !empty($data['film_id']) ? $data['film_id'] : null;
        $this->title        = !empty($data['title']) ? $data['title'] : null;
        $this->description  = !empty($data['description']) ? $data['description'] : null;
        $this->language_id  = 1;
        $this->release_year= !empty($data['release_year']) ? $data['release_year'] : null;
        $this->length      = !empty($data['length']) ? $data['length'] : null;
        $this->last_update = !empty($data['last_update']) ? $data['last_update'] : null;
        $this->original_language_id = !empty($data['original_language_id']) ? $data['original_language_id'] : null;
        $this->rental_duration  = !empty($data['rental_duration']) ? $data['rental_duration'] : null;
        $this->rental_rate      = !empty($data['rental_rate']) ? $data['rental_rate'] : null;
        $this->replacement_cost = !empty($data['replacement_cost']) ? $data['replacement_cost'] : null;
        $this->rating           = !empty($data['rating']) ? $data['rating'] : null;
        $this->special_features = !empty($data['special_features']) ? $data['special_features'] : null;
    }
    
    public function getArrayCopy()
    {
        return [
            'film_id'          => $this->film_id,
            'description' => $this->description,
            'title'       => $this->title,
            'language_id' => $this->language_id,
            'release_year' => $this->release_year,
            'length'      => $this->length,
            'last_update' => $this->last_update, 
            'original_language_id' => $this->original_language_id,
            'rental_duration'   => $this->rental_duration,
            'rental_rate'       => $this->rental_rate,
            'replacement_cost'  => $this->replacement_cost,
            'rating'            => $this->rating,
            'special_features'  => $this->special_features,
        ];
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new DomainException(sprintf(
            '%s does not allow injection of an alternate input filter',
            __CLASS__
        ));
    }

    public function getInputFilter()
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }

        $inputFilter = new InputFilter();

        $inputFilter->add([
            'name' => 'film_id',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);

        $inputFilter->add([
            'name' => 'description',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);


        $inputFilter->add([
            'name' => 'language_id',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);


        $inputFilter->add([
            'name' => 'release_year',
            'required' => false,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);

        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }
}