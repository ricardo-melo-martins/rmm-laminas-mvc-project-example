<?php

namespace Sakila\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Laminas\Paginator\Paginator;

use Sakila\Model\FilmTable;
use Sakila\Form\FilmForm;
use Sakila\Model\Film;

class FilmController extends AbstractActionController
{

    private $table;

    
    public function __construct(FilmTable $table)
    {
        $this->table = $table;
    }
    

    public function indexAction()
    {
        // Grab the paginator from the FilmTable:
        $paginated = true;
        $paginator = $this->table->fetchAll($paginated);
    
        // Set the current page to what has been passed in query string,
        // or to 1 if none is set, or the page is invalid:
        $page = (int) $this->params()->fromQuery('page', 1);
        $page = ($page < 1) ? 1 : $page;

        if($paginator instanceof Paginator){
            $paginator->setCurrentPageNumber($page);
            // Set the number of items per page to 10:
            $countPerPage = (!$paginated ? 0 : 10);
            $paginator->setItemCountPerPage($countPerPage);
        }
        
        return new ViewModel(['paginator' => $paginator]);
    }

    public function addAction()
    {
        $form = new FilmForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $film = new Film();
        $form->setInputFilter($film->getInputFilter());
        $form->setData($request->getPost());
        
        
        if (! $form->isValid()) {

            return ['form' => $form];
        }
        
        $film->exchangeArray($form->getData());
        
        $this->table->saveFilm($film);
        return $this->redirect()->toRoute('film');
    }

    
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('film', ['action' => 'add']);
        }

        // Retrieve the film with the specified id. Doing so raises
        // an exception if the album is not found, which should result
        // in redirecting to the landing page.
        try {
            $film = $this->table->getFilm($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('film', ['action' => 'index']);
        }

        $form = new FilmForm();
        $form->bind($film);
        $form->get('submit')->setAttribute('value', 'Edit');
        
        $viewData = ['id' => $id, 'form' => $form];

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setInputFilter($film->getInputFilter());
        $form->setData($request->getPost());
       
        if (! $form->isValid()) {
            
            return $viewData;
        }

        try {
            $this->table->saveFilm($film);
        } catch (\Exception $e) {
            print_r($e->getMessage()); exit;
        }

        return $this->redirect()->toRoute('film', ['action' => 'index']);
    }


    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('film');
        }
 
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('film_id');
                $this->table->deleteFilm($id);
            }

            return $this->redirect()->toRoute('film');
        }

        return [
            'id'    => $id,
            'film' => $this->table->getFilm($id),
        ];
    }
}